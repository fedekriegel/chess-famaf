#include <iostream>
#include <cstdlib>
#include <utility>
#include "TADPiece.h"

using namespace std;

int main(){

    //system("clear");
    board_t tablero = init_board();
    int x,y,x2,y2;
    bool k = true;


    while(k){
        cout << "---- Chess - Game Started ----" << endl << endl << endl;
        dump_board(tablero);
        cout << "Juega el BLANCO" << endl;
        cout << "Posicion de la pieza a mover: ";
        cin >> x >> y;
        if(x == -1){ k = false;}
        dump_piece(tablero, make_pair(x-1,y-1));
        cout << "Nueva posicion: ";
        cin >> x2 >> y2;
        move_piesa(tablero, make_pair(x-1,y-1), make_pair(x2-1,y2- 1));
       // system("clear");
        dump_board(tablero);
        cout << "Juega el NEGRO" << endl;
        cout << "Posicion de la pieza a mover: ";
        cin >> x >> y;
        if(x == -1){ k = false;}
        dump_piece(tablero, make_pair(x-1,y-1));
        cout << "Nueva posicion: ";
        cin >> x2 >> y2;
        move_piesa(tablero, make_pair(x-1,y-1), make_pair(x2-1,y2- 1));

    }
    free_board(tablero);

    return 0;
}