#ifndef CHESS_TADPIECE_H
#define CHESS_TADPIECE_H

#include <iostream>
#include <cstdlib>
#include <utility>

using namespace std;

typedef enum _colour_t{blanco, negro} colour_t;

typedef enum _name_t{rey, reina, alfil, caballo, torre, peon} name_t;

typedef pair <int,int> position;

typedef struct _piece_t * piece_t;

typedef struct _box_t * box_t;

typedef struct _board_t * board_t;

piece_t init_piece(colour_t color, name_t nombre);	// Inicializa una pieza.

void dump_piece(board_t tabl,position xy);	// Devuelve el nombre de la pieza.

box_t init_box(position xy);	// inicializa la casilla.

void move_piesa(board_t tablero,position xy1, position xy2);	// Mueve una pieza de una casilla a otra.

board_t init_board();	// Inicializa el tablero de juego.

void dump_board(board_t tablero);	// Imprime el tablero de juego.

void free_board(board_t tabl); // Libera de la memoria dinamica todo el tablero.

#endif