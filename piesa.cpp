#include <iostream>
#include <cstdlib>
#include <utility>  // Incluye la función make_pair.
#include <cassert>
#include "TADPiece.h"


#define fs first
#define sd second
#define mk make_pair

using namespace std;

struct _piece_t{
    colour_t color;
    name_t nombre;
};

struct _box_t{
    piece_t pieza;
    bool ocup;
};

struct _board_t {
    box_t tablero[8][8];
};

piece_t init_piece(colour_t c, name_t n){
    auto piece = (piece_t)malloc(sizeof(struct _piece_t));
    assert(piece != nullptr);
    piece->color = c;
    piece->nombre = n;
    return piece;
}

void dump_piece(board_t tabl, position xy){
    name_t n;
    colour_t c;
    assert(tabl != nullptr);
    n = tabl->tablero[xy.first][xy.second]->pieza->nombre;
    c = tabl->tablero[xy.first][xy.second]->pieza->color;
    if(c == blanco){
        if(n == rey){cout << "La pieza seleccionada es: Rey - Kb" << endl;}
        if(n == reina){cout << "La pieza seleccionada es: Reina - Qb" << endl;}
        if(n == alfil){cout << "La pieza seleccionada es: Alfil - Ab" << endl;}
        if(n == caballo){cout << "La pieza seleccionada es: Caballo - Cb" << endl;}
        if(n == torre){cout << "La pieza seleccionada es: Torre - Tb" << endl;}
        if(n == peon){cout << "La pieza seleccionada es: Peon - Pb" << endl;}
    }
    else if(c == negro){
        if(n == rey){cout << "La pieza seleccionada es: Rey - Kn" << endl;}
        if(n == reina){cout << "La pieza seleccionada es: Reina - Qn" << endl;}
        if(n == alfil){cout << "La pieza seleccionada es: Alfil - An" << endl;}
        if(n == caballo){cout << "La pieza seleccionada es: Caballo - Cn" << endl;}
        if(n == torre){cout << "La pieza seleccionada es: Torre - Tn" << endl;}
        if(n == peon){cout << "La pieza seleccionada es: Peon - Pn" << endl;}
    }
}

box_t init_box(){
    auto casilla = (box_t)malloc(sizeof(struct _box_t));
    assert(casilla != nullptr);
    casilla->pieza = nullptr;
    casilla->ocup = false;
    return casilla;
}

board_t init_board(){
    auto tabl = (board_t)malloc(sizeof(struct _board_t));
    assert(tabl != nullptr);
    for( auto &i : tabl->tablero ) {
        for( auto &j : i ) {
            j = init_box();
        }
    }
    for(int i = 0; i < 2; i++){
        for(int j = 0; j < 8 ; j++){
            tabl->tablero[i][j]->ocup = true;
            tabl->tablero[7-i][j]->ocup = true;
        }
    }

    tabl->tablero[0][0]->pieza = init_piece(negro,torre);
    tabl->tablero[0][1]->pieza = init_piece(negro,caballo);
    tabl->tablero[0][2]->pieza = init_piece(negro,alfil);
    tabl->tablero[0][3]->pieza = init_piece(negro,rey);
    tabl->tablero[0][4]->pieza = init_piece(negro,reina);
    tabl->tablero[0][5]->pieza = init_piece(negro,alfil);
    tabl->tablero[0][6]->pieza = init_piece(negro,caballo);
    tabl->tablero[0][7]->pieza = init_piece(negro,torre);
    for(int j = 0; j < 8; j++){
        tabl->tablero[1][j]->pieza = init_piece(negro,peon);
        tabl->tablero[6][j]->pieza = init_piece(blanco,peon);
    }
    tabl->tablero[7][0]->pieza = init_piece(blanco,torre);
    tabl->tablero[7][1]->pieza = init_piece(blanco,caballo);
    tabl->tablero[7][2]->pieza = init_piece(blanco,alfil);
    tabl->tablero[7][3]->pieza = init_piece(blanco,rey);
    tabl->tablero[7][4]->pieza = init_piece(blanco,reina);
    tabl->tablero[7][5]->pieza = init_piece(blanco,alfil);
    tabl->tablero[7][6]->pieza = init_piece(blanco,caballo);
    tabl->tablero[7][7]->pieza = init_piece(blanco,torre);
    return tabl;
}

void dump_board(board_t t){
    piece_t piece = nullptr;
    assert(t != nullptr);
    for(int i = 0; i < 8; i++){
        cout << "   |----|----|----|----|----|----|----|----|" << endl;
        cout << i+1 << "  ";
        for(int j = 0; j < 8 ; j++){cout << "|";
            piece = t->tablero[i][j]->pieza;
            if(piece != nullptr){
                if(piece->color == negro){
                    if(piece->nombre == peon){cout <<" Pn ";}
                    if(piece->nombre == torre){cout <<" Tn ";}
                    if(piece->nombre == rey){cout <<" Kn ";}
                    if(piece->nombre == reina){cout <<" Qn ";}
                    if(piece->nombre == caballo){cout <<" Cn ";}
                    if(piece->nombre == alfil){cout <<" An ";}
                }
                else if(piece->color == blanco){
                    if(piece->nombre == peon){cout <<" Pb ";}
                    if(piece->nombre == torre){cout <<" Tb ";}
                    if(piece->nombre == rey){cout <<" Kb ";}
                    if(piece->nombre == reina){cout <<" Qb ";}
                    if(piece->nombre == caballo){cout <<" Cb ";}
                    if(piece->nombre == alfil){cout <<" Ab ";}
                }
            }
            else {cout << "    ";
            }
        }
        cout << "|" << endl;
    }
    cout << "   |----|----|----|----|----|----|----|----|" << endl;
    cout << "     1    2    3    4    5    6    7    8   " << endl << endl;
}

void free_board(board_t tabl){
    for( auto &i : tabl->tablero ) {
        for( auto &j : i ) {
            free(j->pieza);
            free(j);
        }
    }
    free(tabl);
}

void coronar(board_t a, position ac ){
    cout<<"Elija a que piesa quiere convertir su peon."<<endl;
    cout<<"ingrese 1 -> Reina"<<endl;
    cout<<"ingrese 2 -> Caballo"<<endl;
    cout<<"ingrese 3 -> Torre"<<endl;
    cout<<"ingrese 4 -> Alfil"<<"\n";
    int caso;
    cin>>caso;
    piece_t  pi = a->tablero[ac.fs][ac.sd]->pieza;
    while(caso) {
        switch (caso){
            case 1:
                a->tablero[ac.fs][ac.sd]->pieza = init_piece(pi->color,reina);
                free(pi);
                caso = 0;
                break;
            case 2:
                a->tablero[ac.fs][ac.sd]->pieza = init_piece(pi->color,caballo);
                free(pi);
                caso = 0;
                break;
            case 3:
                a->tablero[ac.fs][ac.sd]->pieza = init_piece(pi->color,torre);
                free(pi);
                caso = 0;
                break;
            case 4:
                a->tablero[ac.fs][ac.sd]->pieza = init_piece(pi->color,alfil);
                free(pi);
                caso = 0;
                break;
            default:
                cout << "balor invalido, ingrese otro numero";
                cin >> caso;
        }
    }
}

bool valid_peon(board_t t,piece_t a,position ac, position next){
    bool res = false;
    if(a->color == negro){
        if(ac.fs == 1){
            if(next.sd == ac.sd){
                if(next.fs == 2 || next.fs == 3 ){
                    return true;
                }
            } else {
                return false;
            }
        } else{
            res = !(t->tablero[next.fs][next.sd]->ocup) && next.fs == ac.fs+1 && next.sd == ac.sd;
            if(res){ return res;}

            res = (t->tablero[next.fs][next.sd]->ocup &&
                         t->tablero[next.fs][next.sd]->pieza->color != a->color &&
                         (next.fs == ac.fs+1 &&(next.sd == ac.sd-1 || next.sd == ac.sd+1)));
            return (res);
        }
    } else {
        if(ac.fs == 6){
            if(next.sd == ac.sd){
                if(next.fs == 5 || next.fs == 4 ){
                    return true;
                }
            } else {
                return false;
            }
        } else{
            res = !(t->tablero[next.fs][next.sd]->ocup) && next.fs == ac.fs-1 && next.sd == ac.sd;
            if(res){ return res;}

            res = (t->tablero[next.fs][next.sd]->ocup &&
                   t->tablero[next.fs][next.sd]->pieza->color != a->color &&
                   (next.fs == ac.fs-1 &&(next.sd == ac.sd-1 || next.sd == ac.sd+1)));
            return (res);
        }
    }
    return res;
}

bool valid_alfil(board_t t,piece_t a,position ac, position next){
    if(ac.fs == next.fs || ac.sd == next.sd){ return false;}//si esta en la misma fila o colum es                                                                    imposible
    bool abajo,izq;
    abajo = ac.fs < next.fs;
    izq =  ac.sd > next.sd;

    int i = ac.fs,j=ac.sd;
    if(abajo){
        if(izq){
            i++;
            j--;
            while(i != next.fs && j != next.sd){
                if(t->tablero[i][j]->ocup){ return false;}
                i++;
                j--;
            }
        } else {
            i++;
            j++;
            while(i != next.fs && j != next.sd){
                if(t->tablero[i][j]->ocup){ return false;}
                i++;
                j++;
            }
        }
    } else {
        if(izq){
            i--;
            j--;
            while(i != next.fs && j != next.sd){
                if(t->tablero[i][j]->ocup){ return false;}
                i--;
                j--;
            }
        } else {
            i--;
            j++;
            while(i != next.fs && j != next.sd){
                if(t->tablero[i][j]->ocup){ return false;}
                i--;
                j++;
            }
        }
    }

    if(t->tablero[next.fs][next.sd]->ocup){
        return !(t->tablero[next.fs][next.sd]->pieza->color == a->color);
    } else {
        return true;
    }
}

bool valid_caballo(board_t t,piece_t a,position ac, position next){
    if(t->tablero[next.fs][next.sd]->ocup && t->tablero[next.fs][next.sd]->pieza->color ==a->color ){
        return false;
    } else {
        if(ac.fs == next.fs + 2 && ac.sd == next.sd +1){ return true;}
        if(ac.fs == next.fs + 2 && ac.sd == next.sd -1){ return true;}
        if(ac.fs == next.fs - 2 && ac.sd == next.sd +1){ return true;}
        if(ac.fs == next.fs - 2 && ac.sd == next.sd -1){ return true;}
        if(ac.fs == next.fs + 1 && ac.sd == next.sd +2){ return true;}
        if(ac.fs == next.fs - 1 && ac.sd == next.sd +2){ return true;}
        if(ac.fs == next.fs + 1 && ac.sd == next.sd -2){ return true;}
        return ac.fs == next.fs - 1 && ac.sd == next.sd - 2;
    }
}

bool valid_torre(board_t t,piece_t a,position ac, position next){

    if(ac.fs != next.fs && ac.sd != next.sd){ return false;}

    bool abajo,izq,igu;
    abajo = ac.fs < next.fs ;
    izq =  ac.sd > next.sd ;
    igu = ac.fs == next.fs;

    cout << (abajo ? "abajo": "arriba");
    cout<<endl;
    cout << (izq ? "izq": "der");
    cout<<endl;


    int i = ac.fs,j=ac.sd;


    if(igu) {
        if(abajo){
            i++;
            while(i != next.fs){
                if(t->tablero[i][j]->ocup){ return false;}
                i++;
            }
        } else {
            i--;
            while(i != next.fs){
                if(t->tablero[i][j]->ocup){ return false;}
                i--;
            }
        }
    } else {
        if(izq){
            j--;
            while(j != next.sd){
                if(t->tablero[i][j]->ocup){ return false;}
                j--;
            }
        } else {
            j++;
            while(j != next.sd){
                if(t->tablero[i][j]->ocup){ return false;}
                j++;
            }
        }
    }

    if(t->tablero[next.fs][next.sd]->ocup){
        return !(t->tablero[next.fs][next.sd]->pieza->color == a->color);
    } else {
        return true;
    }
}

bool is_valid(board_t a, position ac , position next){
    piece_t actual = a->tablero[ac.fs][ac.sd]->pieza;
    if(actual == nullptr){cout << "no hay piesa\n"; return false;}
    bool res=false;

    switch(actual->nombre) {
        case peon:
            res = valid_peon(a,actual,ac,next);
            if(res){
                if(actual->color == blanco){
                    if(next.fs == 0){
                        coronar(a,ac);
                    }
                } else {
                    if(next.fs == 7){
                        coronar(a,ac);
                    }
                }
            }
            break;
        case rey:break;
        case reina:break;
        case alfil:
            res = valid_alfil(a,actual,ac,next);
            break;
        case caballo:
            res = valid_caballo(a,actual,ac,next);
            break;
        case torre:
            res = valid_torre(a,actual,ac,next);
            break;

    }
    return res;
}

bool adentro(position ac){
    return (ac.fs >= 0 && ac.fs < 8 && ac.sd >= 0 && ac.sd < 8);
}

void move_piesa(board_t a ,position ac, position next ){
    
    if(adentro(next) && is_valid(a,ac,next)){
        a->tablero[next.fs][next.sd]->pieza = a->tablero[ac.fs][ac.sd]->pieza;
        a->tablero[next.fs][next.sd]->ocup = true;
        a->tablero[ac.fs][ac.sd]->pieza = nullptr;
        a->tablero[ac.fs][ac.sd]->ocup = false;
    } else {
        cout << "No es valido"<<endl;
    }


}





